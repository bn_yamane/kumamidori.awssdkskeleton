<?php

namespace Kumamidori\AwsSdkSkeleton;

class AwsSdkSkeletonTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AwsSdkSkeleton
     */
    protected $skeleton;

    protected function setUp()
    {
        $this->skeleton = new AwsSdkSkeleton;
    }

    public function testNew()
    {
        $actual = $this->skeleton;
        $this->assertInstanceOf('\Kumamidori\AwsSdkSkeleton\AwsSdkSkeleton', $actual);
    }

    /**
     * @expectedException \Kumamidori\AwsSdkSkeleton\Exception\LogicException
     */
    public function testException()
    {
        throw new Exception\LogicException;
    }
}
