<?php
/**
 * This file is part of the Kumamidori.AwsSdkSkeleton
 *
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace Kumamidori\AwsSdkSkeleton\Exception;

/**
 * RuntimeException
 *
 * @package Kumamidori.AwsSdkSkeleton
 */
class RuntimeException extends \LogicException implements ExceptionInterface
{
}
