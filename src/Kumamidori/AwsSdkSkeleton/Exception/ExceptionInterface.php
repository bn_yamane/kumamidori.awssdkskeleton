<?php
/**
 * This file is part of the Kumamidori.AwsSdkSkeleton
 *
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace Kumamidori\AwsSdkSkeleton\Exception;

/**
 * Generic package exception
 *
 * @package Kumamidori.AwsSdkSkeleton
 */
interface ExceptionInterface
{
}
