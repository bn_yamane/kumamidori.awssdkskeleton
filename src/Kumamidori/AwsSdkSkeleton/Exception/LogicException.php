<?php
/**
 * This file is part of the Kumamidori.AwsSdkSkeleton
 *
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace Kumamidori\AwsSdkSkeleton\Exception;

/**
 * LogicException
 *
 * @package Kumamidori.AwsSdkSkeleton
 */
class LogicException extends \LogicException implements ExceptionInterface
{
}
