<?php
/**
 * This file is part of the Kumamidori.AwsSdkSkeleton
 *
 * @license http://opensource.org/licenses/bsd-license.php BSD
 */
namespace Kumamidori\AwsSdkSkeleton;

use Dotenv;

/**
 * Kumamidori.AwsSdkSkeleton
 *
 * @package Kumamidori.AwsSdkSkeleton
 */
class AwsSdkSkeleton
{
    public function execute()
    {
        $path = __DIR__ . '/../../../';
        Dotenv::load($path);
        var_dump($_SERVER);
    }
}
