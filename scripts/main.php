<?php

require __DIR__ . '/../vendor/autoload.php';

use Kumamidori\AwsSdkSkeleton\AwsSdkSkeleton;

$main = new AwsSdkSkeleton();
$main->execute();
